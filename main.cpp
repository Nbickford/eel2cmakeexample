// Adapted from WDL's loose_eel.cpp, part of WDL (c) 2005 Cockos Incorporated
// Modified for post on building EEL2 by Neil Bickford, (c) 2021 Neil Bickford
// SPDX-License-Identifier: zlib
#include <cstdarg> // For varargs
#include <iostream> // For std::cerr and std::cout

// Define a printf-like function for writing warning and error messages
// before including EEL.
static void writeToStandardError(const char* fmt, ...)
{
	va_list arglist;
	va_start(arglist, fmt);
	vfprintf(stderr, fmt, arglist);
	fprintf(stderr, "\n");
	fflush(stderr);
	va_end(arglist);
}

#define EEL_STRING_DEBUGOUT writeToStandardError

// Main EEL file
#include "eelscript.h"

// These are used by various EEL functions; I don't know what they do,
// but they seem to be used to serialize calls to library functions with
// global state (i.e. that aren't thread-safe, such as CRT's qsort).
//
// My best guess is that these must be non-empty when multiple threads
// in the process can compile EEL scripts at once. In that case,
// I'm guessing (on Windows) one would need to create a mutex using
// CreateMutex before multi-threaded compilation, and then
// ...EnterMutex would call WaitForSingleObject and LeaveMutex would
// call ReleaseMutex, following the Using Mutex Objects documentation
// at https://docs.microsoft.com/en-us/windows/win32/sync/using-mutex-objects.
// However, I haven't been able to find any examples of these functions
// with non-empty bodies online.
void NSEEL_HOSTSTUB_EnterMutex() { }
void NSEEL_HOSTSTUB_LeaveMutex() { }

int main() {
	// Initialize EEL
	if (eelScriptInst::init()) {
		std::cerr << "eelScriptInst::init() failed!\n";
		return -1;
	}

	const char* code = "printf(\"Hello world from inside EEL!\\n\");";

	eelScriptInst inst;
	inst.runcode(code, // const char* codeptr
		2, // int showerr
		"__cmdline__", // const char* showerrfn
		true, // bool canfree
		true, // bool ignoreEndOfInputChk
		true); // bool doExec

	std::cout << "Hello world from outside EEL!\n";
}